---
title: What is Mothership?
date: 2017-11-02
---

All aboard the mothership!

...

Well, what is it?

[Mothership](https://mothership.cx/) (MSP) is a relatively new cryptocurrency produced by the Mothership Foundation, whose goal is to deliver a variety of services to business owners through the use of their cloud platform and the Ethereum blockchain. Their mission statement is as follows:

```
Mothership is democratizing the funding for new blockchain projects, making cryptocurrency markets accessible for global blockchain companies and providing everything necessary for launching new ones. Our custom-made legal toolkit and marketplace simplifies selling digital assets and makes it easy and safe for supporters to invest.```

Their target market appears to be startups who want to open business in the EU (European Union), particularly in Estonia. They plan to integrate with Estonia's [e-Resident program](https://e-resident.gov.ee/), which enables internationals to become a virtual resident of Estonia and operate their Estonia-based business and pay taxes online. As this is a government program, and Mothership appears to have endorsement from [at least one](https://www.linkedin.com/in/oleggutsol/) of the members of the e-Residency team, there may be some worth-while value in the services they offer.

From their very concise [whitepaper](https://mothership.cx/documents/whitepaper.pdf), here are some of the services they offer:
- A token market for the EU (European Union), democratising funding for new blockchain projects
- A new wallet, connected to your e-Resident digital identity
- A secure, stable cryptocurrency exchange with additional features for e-Residents and EU businesses
- A blockchain and exchange connected hosting platform for your own
applications

In other words, they will provide a means for democratic funding for new businesses, a tailored cryptocurrency wallet that functions hand-in-hand with one's e-Resident identity, a means for trading between currencies on a Mothership-owned cryptocurrency exchange, and a hosting platform for applications with API-access to the blockchain and Mothership exchange.

On the surface, these seem like very typical services that one could get anywhere else online. There are plenty of exchanges and cloud hosting platforms out there already. Perhaps the strong suit of Mothership's proposal is the fact that it integrates with Estonia's e-Residency program. Aside from this, there is little incentive to get involved with their platform.

Onto the currency. The currency of Mothership, MSP (is this also pronounced "mothership"?), is a clone of the MiniMe ERC20 token, so while it does have some additional [features](https://github.com/Giveth/minime), the coin itself doesn't bring anything novel to the world of cryptocurrency.

Up-to-date stats on the currency can be found [here](https://coinmarketcap.com/currencies/mothership/):


